package com.sunzn.watcher;

import android.text.Editable;
import android.text.TextWatcher;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class WatcherViewModel extends ViewModel {

    private MutableLiveData<String> mValueLiveData;

    public LiveData<String> getData() {
        if (mValueLiveData == null) {
            mValueLiveData = new MutableLiveData<>();
        }
        return mValueLiveData;
    }

    public void setData(String value) {
        if (mValueLiveData != null) {
            mValueLiveData.setValue(value);
        }
    }

    public String getValue() {
        return getData().getValue();
    }

    @Override
    public void onCleared() {
        super.onCleared();
        mValueLiveData = null;
    }

    public TextWatcher textWatcher = new WatcherProxy() {
        @Override
        public void afterTextChanged(Editable s) {
            setData(s.toString());
        }
    };

}
