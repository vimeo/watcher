package com.sunzn.watcher;

import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.sunzn.watcher.databinding.ActivityWatcherBinding;

public class WatcherActivity extends AppCompatActivity implements View.OnClickListener {

    private ActivityWatcherBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_watcher);
        binding.setOnClickListener(this);

        WatcherViewModel viewModel = new ViewModelProvider(this).get(WatcherViewModel.class);
        binding.setModel(viewModel);
        viewModel.getData().observe(this, new Observer<String>() {
            @Override
            public void onChanged(String s) {
                binding.editClean.setVisibility(s.length() == 0 ? View.GONE : View.VISIBLE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        binding.editText.setText("");
    }

}